/**
 * Created by ArtTim on 19.08.2017.
 */

var pjs = new PointJS('2d', 400, 400);
pjs.system.initFullPage();

var game = pjs.game;
var point = pjs.vector.point;

var key = pjs.keyControl;
key.initKeyControl();

var mouse = pjs.mouseControl;
mouse.initMouseControl();
var asphalt = game.newBackgroundObject({
    file: 'images/asphalt.jpg',
    countX: 10,
    countY: 10,
    w: 100, h: 100
});
var car = game.newImageObject({
    file: 'images/car.png',
    scale: 0.1,
    angle: -90
})
var playerTile = pjs.tiles.newImage('images/man2.png');
var playerAnimation = playerTile.getAnimation(0, 0 , 70, 70, 7);
var player = game.newAnimationObject({
    animation: playerAnimation,
    delay: 10,
    x: 100,
    y: 100,
    w: 50, h: 50
});
car.setCenter(-50,0);
var speed = 0;
var maxspeed = 8;
game.newLoop('game', function () {
    game.fill('#D9D9D9');
    pjs.camera.setPositionC(car.getPosition(101));
    player.setCenter(-10, 0);
    player.rotate(mouse.getPosition());

    var step = 0.03;
    if (key.isDown('W')) {
        speed += speed < maxspeed ? 0.03 : 0;

    }
    else if (key.isDown('S')) {
        speed -= speed > -maxspeed / 2 ? step : 0;

    } else {
        if (speed != 0) {
            if (speed > 0) speed -= step * 2;
            if (speed < 0) speed += step * 2;
        }
    }
    if (speed != 0) {
        car.moveAngle(speed);
        if (key.isDown('A'))
            car.turn(-speed / 2);
        if (key.isDown('D'))
            car.turn(speed / 2);
    }


    asphalt.draw();
    car.draw();


    if (player.isIntersect(car)) {

        player.moveAngle(5);
        player.drawFrame(7);

    }
    else if (key.isDown('UP')) {
        player.drawFrames(1, 6);
        player.moveAngle(1.5);

    }
    else
        player.drawFrame(0);
    player.moveAngle(0);

});

game.startLoop('game');
















































