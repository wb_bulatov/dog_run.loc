/**
 * Created by ArtTim on 19.08.2017.
 */
var bs = require("browser-sync").create();

bs.init({
    server: "./"
});
bs.watch('./*.html').on('change', bs.reload);