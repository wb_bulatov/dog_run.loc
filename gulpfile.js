/**
 * Created by ArtTim on 19.08.2017.
 */
var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');

gulp.task('serve',['sass'], function () {

    browserSync.init({
        server: './'
    });
    gulp.watch('./styles/scss/*.scss', ['sass']);
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('./js/*.js').on('change', browserSync.reload);

});
gulp.task('sass', function () {
    return gulp.src('./styles/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./styles/css'))
        .pipe(browserSync.stream());
});


gulp.task('default', ['serve']);